#!/usr/bin/env python3
import copy


class Intcode_Computer():
    def __init__(self, memory, pointer = 0):
        self.memory = memory
        self.pointer = pointer


    def set_memory(self, memory):
        self.memory = copy.deepcopy(memory)


    def store_at_address(self, value, address):
        self.memory[address] = value


    def set_pointer(self, pointer):
        self.pointer = pointer


    def set_noun(self, noun):
        self.memory[1] = noun


    def set_verb(self, verb):
        self.memory[2] = verb


    def increment_pointer(self, increment):
        self.pointer += increment

    
    def analyse_opcode(self, num_parameters):
        opcode_str = str(self.memory[self.pointer])
        opcode_str_reversed = list(opcode_str[::-1])
        modes = []
        for pos in range(2, num_parameters + 1):
            try:
                modes.append(int(opcode_str_reversed[pos]))
            except IndexError:
                modes.append(0)
        
        return modes


    def op_code_1(self):
        num_of_values = 4
        modes = self.analyse_opcode(num_of_values)
        
        value_1_location = self.memory[self.pointer + 1]
        if modes[0] == 0:
            value_1 = self.memory[value_1_location]
        elif modes[0] == 1:
            value_1 = value_1_location
        
        value_2_location = self.memory[self.pointer + 2]
        if modes[1] == 0:
            value_2 = self.memory[value_2_location]
        elif modes[1] == 1:
            value_2 = value_2_location
        
        store_location = self.memory[self.pointer + 3]
        
        store_value = value_1 + value_2
        self.store_at_address(store_value, store_location)
        self.increment_pointer(num_of_values)


    def op_code_2(self):
        num_of_values = 4
        modes = self.analyse_opcode(num_of_values)

        value_1_location = self.memory[self.pointer + 1]
        if modes[0] == 0:
            value_1 = self.memory[value_1_location]
        elif modes[0] == 1:
            value_1 = value_1_location
        
        value_2_location = self.memory[self.pointer + 2]
        if modes[1] == 0:
            value_2 = self.memory[value_2_location]
        elif modes[1] == 1:
            value_2 = value_2_location

        store_location = self.memory[self.pointer + 3]
        
        store_value = value_1 * value_2
        self.store_at_address(store_value, store_location)
        self.increment_pointer(num_of_values)


    def op_code_3(self):
        num_of_values = 2
        input_value = int(input("Integer please: "))
        location = self.memory[self.pointer + 1]
        self.memory[location] = input_value
        self.increment_pointer(num_of_values)


    def op_code_4(self):
        num_of_values = 2
        modes = self.analyse_opcode(num_of_values)

        output_location = self.memory[self.pointer + 1]
        if modes[0] == 0:
            output = self.memory[output_location]
        elif modes[0] == 1:
            output = output_location

        print(output)
        self.increment_pointer(num_of_values)


    def op_code_5(self):
        num_of_values = 3
        modes = self.analyse_opcode(num_of_values)

        parameter_1_location = self.memory[self.pointer + 1]
        if modes[0] == 0:
            parameter_1 = self.memory[parameter_1_location]
        elif modes[0] == 1:
            parameter_1 = parameter_1_location

        parameter_2_location = self.memory[self.pointer + 2]
        if modes[1] == 0:
            parameter_2 = self.memory[parameter_2_location]
        elif modes[1] == 1:
            parameter_2 = parameter_2_location

        if parameter_1 != 0:
            self.set_pointer(parameter_2)
        else:
            self.increment_pointer(num_of_values)


    def op_code_6(self):
        num_of_values = 3
        modes = self.analyse_opcode(num_of_values)

        parameter_1_location = self.memory[self.pointer + 1]
        if modes[0] == 0:
            parameter_1 = self.memory[parameter_1_location]
        elif modes[0] == 1:
            parameter_1 = parameter_1_location

        parameter_2_location = self.memory[self.pointer + 2]
        if modes[1] == 0:
            parameter_2 = self.memory[parameter_2_location]
        elif modes[1] == 1:
            parameter_2 = parameter_2_location

        if parameter_1 == 0:
            self.set_pointer(parameter_2)
        else:
            self.increment_pointer(num_of_values)


    def op_code_7(self):
        num_of_values = 4
        modes = self.analyse_opcode(num_of_values)

        value_1_location = self.memory[self.pointer + 1]
        if modes[0] == 0:
            value_1 = self.memory[value_1_location]
        elif modes[0] == 1:
            value_1 = value_1_location

        value_2_location = self.memory[self.pointer + 2]
        if modes[1] == 0:
            value_2 = self.memory[value_2_location]
        elif modes[1] == 1:
            value_2 = value_2_location

        store_location = self.memory[self.pointer + 3]

        if value_1 < value_2:
            store = 1
        else:
            store = 0

        self.store_at_address(store, store_location)
        self.increment_pointer(num_of_values)


    def op_code_8(self):
        num_of_values = 4
        modes = self.analyse_opcode(num_of_values)

        value_1_location = self.memory[self.pointer + 1]
        if modes[0] == 0:
            value_1 = self.memory[value_1_location]
        elif modes[0] == 1:
            value_1 = value_1_location

        value_2_location = self.memory[self.pointer + 2]
        if modes[1] == 0:
            value_2 = self.memory[value_2_location]
        elif modes[1] == 1:
            value_2 = value_2_location

        store_location = self.memory[self.pointer + 3]

        if value_1 == value_2:
            store = 1
        else:
            store = 0

        self.store_at_address(store, store_location)
        self.increment_pointer(num_of_values)


    def op_code_99(self):
        return self.memory


    def run(self):
        while True:
            opcode = str(self.memory[self.pointer])[-1]
            if opcode == "1":
                self.op_code_1()
                continue
            elif opcode == "2":
                self.op_code_2()
                continue
            elif opcode == "3":
                self.op_code_3()
                continue
            elif opcode == "4":
                self.op_code_4()
                continue
            elif opcode == "5":
                self.op_code_5()
                continue
            elif opcode == "6":
                self.op_code_6()
                continue
            elif opcode == "7":
                self.op_code_7()
                continue
            elif opcode == "8":
                self.op_code_8()
                continue
            elif opcode == "9":
                return self.op_code_99()
            else:
                print("Halting and catching fire\n1202")
                exit(1202)


if __name__ == "__main__":
    INITIAL_MEMORY = []
    INITIAL_MEMORY_FILE = "/Users/kyle/git/adventofcode/2019/Day 5/initial_memory.csv"

    with open(INITIAL_MEMORY_FILE, 'r') as mem:
        for address in mem.read().split(','):
            INITIAL_MEMORY.append(int(address))

    intcode_computer = Intcode_Computer(INITIAL_MEMORY)
    intcode_computer.run()
