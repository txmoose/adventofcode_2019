#!/usr/bin/env python


orbits = { 'COM': {'direct': None, 'indirect': 0},
            'B': {'direct': 'COM', 'indirect': 0},
            'C': {'direct': 'B', 'indirect': 1},
            'D': {'direct': 'C', 'indirect': 2},
            'E': {'direct': 'D', 'indirect': 3},
            'G': {'direct': 'B', 'indirect': 1},
            'H': {'direct': 'G', 'indirect': 2}}


orbits = {}

with open('orbit_map.txt', 'r') as orbit_map_txt:
    orbit_map = orbit_map_txt.readlines()


for orbit in orbit_map:
    orbit_left, orbit_right = orbit.strip().split(')')
    orbits[orbit_left] = {'direct': 0}


def calculate_indrect_orbits(orbit_left):
    if orbit[orbit_left]['direct'] is None:
        return 0
    else:
        return 1 + calculate_indrect_orbits(orbit[orbit_left]['direct'])


