#!/usr/bin/env python

puzzle_range = (165432, 707912)
count = []

for attempt in range(puzzle_range[0], puzzle_range[1] + 1):
    match = False
    attempt_str = str(attempt)

    attempt_str_sorted = "".join(sorted(str(attempt)))
    if attempt_str == attempt_str_sorted:
            match = True

    if match:
        match = False
        for i in range(6):
            if i == 5:
                break

            if attempt_str[i] == attempt_str[i + 1] and attempt_str.count(attempt_str[i]) % 2 == 0:
                if attempt_str.count(attempt_str[i]) != 2:
                    continue

                match = True
                break
    else:
        continue

    if match:
        #print(attempt)
        count.append(attempt)


print(f"{len(count)} possible answers")